## Test task

---

**How to install and use this project**

Clone repository from bitbucket to your computer using GIT:
*git clone git@bitbucket.org:olenashramko/web.git*.
;)

1. **cd** inside folder with project and do `docker-compose up` for uping our docker environment *(I hope you have docker installed;))*
2. Go inside your php container and do `composer install` *(It might be smth like this `docker exec -it php /bin/sh` to switch inside your php container)*
3. **This is optional** - inside your php container do `bin/console insert-users:run` - this command will create users with funny names and bios reading them from json file
4. For downloading all posts from api *jsonplaceholder* to mongodb write in your php container `bin/console download-posts:run`
5. Hooray - you have almost done!
6. There are two endpoints to get all info you'd needed:
   - `/api/count` - for getting info about how many posts does every user have
   - `/api/user-posts/{id}` - for getting all posts of concrete user by his id
7. So go to your browser and write `http://localhost/api/user-posts/count` - you will get list of users with their ids, so you can use those ids for another url of the app
8. I wish everything was working well;)
9. For any additional info write to my email or telegram.


---