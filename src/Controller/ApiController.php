<?php

namespace App\Controller;

use App\Document\Post;
use App\Document\User;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ApiController
 * @package App\Controller\Api
 * @Route("api/")
 */
class ApiController extends AbstractController
{
    /**
     * @param string $id
     * @param Request $request
     * @param DocumentManager $dm
     * @return JsonResponse
     * @Route("user-posts/{id}", name="user.posts")
     */
    public function getUserPosts(string $id, Request $request, DocumentManager $dm): JsonResponse
    {
        $user = $dm->getRepository(User::class)->findOneBy(['id' => $id]);
        if (is_null($user)) {
            throw new NotFoundHttpException('User with such id is not found');
        }
        $result = [];
        /**
         * @var Post $post
         */
        foreach ($user->getPosts()->toArray() as $post){
            $result[] = $post->showInfo();
        }
        return $this->json($result);
    }

    /**
     * @param Request $request
     * @param DocumentManager $dm
     * @return JsonResponse
     * @Route("count", name="count.posts")
     */
    public function getCountPosts(Request $request, DocumentManager $dm): JsonResponse
    {
        $response = [];
        $users = $dm->getRepository(User::class)->findAll();
        foreach ($users as $user) {
            $response[] = [
                'id' => $user->getId(),
                'countPosts' => $user->countPosts()
            ];
        }
        return $this->json($response);
    }
}
