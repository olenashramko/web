<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class AbstractDocument
 * @package App\Document
 */
class AbstractDocument
{
    /**
     * @ODM\Id(strategy="NONE")
     * @var string
     */
    protected string $id;

    /**
     * @return string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }
}