<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\ODM\MongoDB\PersistentCollection;

/**
 * Class User
 * @package App\Document
 * @ODM\Document()
 */
class User extends AbstractDocument
{
    /**
     * @ODM\Field(type="string")
     * @var string|null
     */
    protected string $bio = '';

    /**
     * @ODM\Field(type="string")
     * @var string
     */
    protected string $username;

    /**
     * @var PersistentCollection|Post[]|null
     * @ODM\ReferenceMany(targetDocument=Post::class, mappedBy="userId")
     */
    protected $posts = [];

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return Post[]|PersistentCollection|null
     */
    public function getPosts()
    {
        return $this->posts;
    }

    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->setUserId($this);
        }

        return $this;
    }

    public function countPosts(): int
    {
        return count($this->posts);
    }
    /**
     * @param Post[]|PersistentCollection|null $posts
     */
    public function setPosts($posts): void
    {
        $this->posts = $posts;
    }

    /**
     * @return string
     */
    public function getBio(): string
    {
        return $this->bio;
    }

    /**
     * @param string $bio
     */
    public function setBio(string $bio): void
    {
        $this->bio = $bio;
    }

}