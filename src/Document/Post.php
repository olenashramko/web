<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\ODM\MongoDB\Mapping\ClassMetadata;

/**
 * Class Post
 * @package App\Document
 * @ODM\Document()
 */
class Post extends AbstractDocument
{
    /**
     * @var string
     * @ODM\Field(type="string")
     */
    protected string $title;
    /**
     * @ODM\Field(type="string")
     * @var string|null
     */
    protected ?string $body;
    /**
     * @var null|User
     * @ODM\ReferenceOne(targetDocument=User::class, storeAs=ClassMetadata::REFERENCE_STORE_AS_ID, inversedBy="posts")
     * @ODM\Index()
     */
    protected ?User $userId;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }


    /**
     * @return array[]
     */
    public function showInfo(): array
    {
        return [
            $this->id => [
                'title' => $this->title,
                'body' => $this->body
            ]
        ];
    }

    /**
     * @return string|null
     */
    public function getBody(): ?string
    {
        return $this->body;
    }

    /**
     * @param string|null $body
     */
    public function setBody(?string $body): void
    {
        $this->body = $body;
    }

    /**
     * @return User|null
     */
    public function getUserId(): ?User
    {
        return $this->userId;
    }

    /**
     * @param User|null $userId
     */
    public function setUserId(?User $userId): void
    {
        $this->userId = $userId;
    }
}