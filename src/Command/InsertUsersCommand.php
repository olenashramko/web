<?php

namespace App\Command;

use App\Document\User;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\MongoDBException;
use GuzzleHttp\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InsertUsersCommand extends Command
{
    private Client $guzzleClient;

    private string $jsonPostLink;

    private DocumentManager $dm;

    /**
     * DownloadPostsCommand constructor.
     * @param Client $guzzleClient
     * @param string $jsonPostLink
     */
    public function __construct(Client $guzzleClient, string $jsonPostLink, DocumentManager $dm)
    {
        parent::__construct();
        $this->guzzleClient = $guzzleClient;
        $this->jsonPostLink = $jsonPostLink;
        $this->dm = $dm;
    }

    protected function configure(): void
    {
        $this
            ->setName('insert-users:run')
            ->setDescription('Run this to insert users if your table is empty');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws MongoDBException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $users = $this->dm->getRepository(User::class)
            ->findAll();
        if (count($users) == 0) {
            $usersJson = file_get_contents(__DIR__ . '/users.json'); //read from file
            $users = json_decode($usersJson, true);
            foreach ($users as $item) {
                $product = new User();
                $product->setId($item['id']);
                $product->setUsername($item['username']);
                $product->setBio($item['bio'] ?? '');
                $this->dm->persist($product);
            }
            $this->dm->flush();
            $output->writeln('Done!');
            return 1;
        }
        $output->writeln('You already have users');
        return 1;
    }
}
