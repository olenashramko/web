<?php

namespace App\Command;

use App\Document\Post;
use App\Document\User;
use Doctrine\ODM\MongoDB\DocumentManager;
use GuzzleHttp\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DownloadPostsCommand extends Command
{
    private Client $guzzleClient;

    private string $jsonPostLink;

    private DocumentManager $dm;

    /**
     * DownloadPostsCommand constructor.
     * @param Client $guzzleClient
     * @param string $jsonPostLink
     * @param DocumentManager $dm
     */
    public function __construct(Client $guzzleClient, string $jsonPostLink, DocumentManager $dm)
    {
        parent::__construct();
        $this->guzzleClient = $guzzleClient;
        $this->jsonPostLink = $jsonPostLink;
        $this->dm = $dm;
    }

    protected function configure(): void
    {
        $this
            ->setName('download-posts:run')
            ->setDescription('Download posts to mongodb from jsonplaceholder.typicode.com/posts');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $res = $this->guzzleClient->request('GET', $this->jsonPostLink);
        if ($res->getStatusCode() == 200){
            $result = json_decode($res->getBody(), true);
            foreach ($result as $item){
                $user = $this->dm->getRepository(User::class)->findOneBy(['id' => (string)$item['userId']]);
                if (!$user){
                    $user = new User();
                    $user->setId((string)$item['userId']);
                }
                $post = new Post();
                $post->setId($item['id']);
                $post->setBody($item['body']);
                $post->setTitle($item['title']);
                $user->addPost($post);
                $this->dm->persist($post);
                $this->dm->persist($user);
            }
            $this->dm->flush();
            $output->writeln('Done!');
            return 1;
        }
        $output->writeln('Smth went wrong!');
        return 1;
    }
}
